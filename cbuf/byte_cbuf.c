#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "byte_cbuf.h"

#define INIT_MAGIC_NUM 0xDEADBEEF

void byte_cbuf_init(byte_cbuf_t* cbuf,uint8_t* buffer,uint16_t size)
{
	if(!cbuf || !buffer)
		return;

	if(cbuf->init_magic_num == INIT_MAGIC_NUM)
		return;

	cbuf->buffer = buffer;
	cbuf->size = size;

	cbuf->length = 0;
	cbuf->read_index = 0;
	cbuf->write_index = 0;

	cbuf->init_magic_num = INIT_MAGIC_NUM;
}
bool byte_cbuf_push(byte_cbuf_t* cbuf,uint8_t data)
{
	if(!cbuf)
		return false;

	if(cbuf->init_magic_num != INIT_MAGIC_NUM)
		return false;

	if(cbuf->length == cbuf->size)
		return false; // no space available

	cbuf->buffer[cbuf->write_index] = data;
	cbuf->write_index = ((cbuf->write_index + 1) % cbuf->size);
	cbuf->length++;

	return true;

}
bool byte_cbuf_pop(byte_cbuf_t* cbuf,uint8_t* data)
{
	if(!cbuf)
		return false;

	if(cbuf->init_magic_num != INIT_MAGIC_NUM)
		return false;

	if(cbuf->length == 0)
		return false;

	*data = cbuf->buffer[cbuf->read_index];
	cbuf->buffer[cbuf->read_index] = 0;
	cbuf->read_index = ((cbuf->read_index + 1) % cbuf->size);
	cbuf->length--;

	return true;
}
bool byte_cbuf_is_empty(byte_cbuf_t* cbuf)
{
	if(!cbuf)
		return false;

	if(cbuf->init_magic_num != INIT_MAGIC_NUM)
		return false;

	if(cbuf->length == 0)
		return true; //cbuf is empty

	return false;
}

void byte_cbuf_print_details(byte_cbuf_t* cbuf)
{
	if(!cbuf)
		return;

	if(cbuf->init_magic_num != INIT_MAGIC_NUM)
		return;

	printf("\n");
	printf("length:%d(size:%d)\n",cbuf->length,cbuf->size);

	for(uint16_t i=0; i < cbuf->size; i++){
		if(i == cbuf->read_index)
			printf("   R");
		else
			printf("   ");
	}
	printf("\n");

	//print data
	for(uint16_t i=0; i < cbuf->size; i++)
		printf("%4d",cbuf->buffer[i]);
	printf("\n");

	//print write pointer
	for(uint16_t i=0; i < cbuf->size; i++){
		if(i == cbuf->write_index)
			printf("    W");
		else
			printf("   ");
	}
	printf("\n");
}




