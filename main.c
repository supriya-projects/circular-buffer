#include <stdio.h>
#include <stdint.h>
#include "cbuf/byte_cbuf.h"

#define BUFFER_SIZE_BYTES 10

static uint8_t __buffer[BUFFER_SIZE_BYTES];
static byte_cbuf_t __cbuf;

static void __add_data(uint8_t data);
static void __remove_data(void);

int main(void)
{
	byte_cbuf_init(&__cbuf, __buffer, BUFFER_SIZE_BYTES);
	byte_cbuf_print_details(&__cbuf);

	__add_data(10);
	__add_data(11);
	__add_data(12);
	byte_cbuf_print_details(&__cbuf);

	__remove_data();
	byte_cbuf_print_details(&__cbuf);

	__add_data(20);
	__add_data(21);
	__add_data(22);
	byte_cbuf_print_details(&__cbuf);

	__remove_data();
	__remove_data();
	byte_cbuf_print_details(&__cbuf);

	return 0;
}

static void __add_data(uint8_t data)
{
	bool result;

	result = byte_cbuf_push(&__cbuf, data);
	if(result == false)
		printf("failed to add data\n");
	else
		printf("adding:%d\n",data);
}

static void __remove_data(void)
{
	uint8_t data;
	bool result;
	result = byte_cbuf_pop(&__cbuf, &data);
	if(result == false)
		printf("failed to remove data\n");
}
